# JobAndTalent task by AivanF

The app provides a couple of endpoints to check
if two developers are connected ob both
Twitter (following each other) and
GitHub (having some common organisations).
For now, due to lack of API access, mock integrations are used,
but the logic depends on abstract adapter classes,
so it's easy to add new implementations.

Tech stack: Python 3.9, FastAPI, SQLAlchemy, PyTest,
SQLite (useful for testing), PostgreSQL, Docker-Compose.
In addition, cool networkx library is used to emulate
Twitter social network.

## Run service

```bash
docker-compose up --build
# Here, Postgres will be used

# Or manually:
pip install -r requirements.txt
python app.py
# Port and host may be specified:
python app.py -P 4321 -H 0.0.0.0
# By default, SQLite will be used
```

OpenAPI docs available at http://127.0.0.1/docs

## Run tests

```bash
docker-compose -f docker-compose.test.yml up --build

# Or manually:
pip install -r requirements.txt
pytest tests/
```

## TODO
- Review logic splitting between `app.py` and `controller.py`.
- Implement real API integrations instead of mocks.
- In some cases of future development,
  it may be useful to encapsulate 3rd-party integrations
  into separate containers aka side-car architectural pattern.
- Create more complex test cases,
  consider pipelines of data changes events.
