__author__ = "AivanF"

from typing import Optional, List
from os import environ
from datetime import datetime
from argparse import ArgumentParser

import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from controller import Controller
from adapters.github_mock import create_mock_github
from adapters.twitter_mock import create_mock_twitter

from db.config import async_session, create_database
from db.crud import create_dev_connection, get_dev_connections

from utils import datetime_isoformat

app = FastAPI()
processor = Controller()


@app.on_event("startup")
async def startup():
    processor.init(create_mock_github(), create_mock_twitter())
    await create_database()


class UserConnection(BaseModel):
    connected: bool
    organisations: Optional[List[str]]


class UserConnectionError(BaseModel):
    errors: List[str]


@app.get("/connected/realtime/{dev1}/{dev2}",
         response_model=UserConnection,
         responses={
             200: {"model": UserConnection},
             404: {"model": UserConnectionError},
         })
async def get_connected_realtime(dev1: str, dev2: str):
    common_gh_org, errors = await processor.get_connected_realtime(dev1, dev2)
    connected = bool(common_gh_org)
    if errors:
        code = 404
        result = {"errors": errors}
    else:
        code = 200
        result = {"connected": connected}
        if connected:
            result["organisations"] = common_gh_org
        async with async_session() as session:
            async with session.begin():
                await create_dev_connection(session, dev1, dev2, datetime.utcnow(), result)
    return JSONResponse(status_code=code, content=result)


class SavedUserConnection(UserConnection):
    registered_at: str


@app.get("/connected/register/{dev1}/{dev2}", response_model=List[SavedUserConnection])
async def get_connected_register(dev1: str, dev2: str) -> List[dict]:
    async with async_session() as session:
        async with session.begin():
            objects = await get_dev_connections(session, dev1, dev2)
    result = []
    for obj in objects:
        result.append({
            **obj.data,
            "registered_at": datetime_isoformat(obj.registered_at),
        })
    return result


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        "--host", "-H", type=str,
        default=environ.get("LISTEN_HOST", "127.0.0.1"))
    parser.add_argument(
        "--port", "-P", type=int,
        default=int(environ.get("LISTEN_PORT", 4321)))
    args = parser.parse_args()
    uvicorn.run("app:app", port=args.port, host=args.host)
