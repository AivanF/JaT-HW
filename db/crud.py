__author__ = "AivanF"

from datetime import datetime

from sqlalchemy.future import select
from sqlalchemy.orm import Session

from db.models import DevConnection


def sort_ids(dev1, dev2):
    ids = dev1, dev2
    dev1, dev2 = min(ids), max(ids)
    return dev1, dev2


async def create_dev_connection(db: Session, dev1: str, dev2: str, registered_at: datetime, data: dict):
    dev1, dev2 = sort_ids(dev1, dev2)
    connection = DevConnection(dev1=dev1, dev2=dev2, registered_at=registered_at, data=data)
    db.add(connection)
    await db.flush()


async def get_dev_connections(db: Session, dev1: str, dev2: str):
    dev1, dev2 = sort_ids(dev1, dev2)
    raw_q = select(DevConnection) \
        .where(DevConnection.dev1 == dev1, DevConnection.dev2 == dev2) \
        .order_by(DevConnection.id)
    q = await db.execute(raw_q)
    return q.scalars().all()
