__author__ = "AivanF"

from os import environ
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker

DATABASE_URL = environ.get("DATABASE_URL", "sqlite+aiosqlite:///./local.db")
engine = create_async_engine(DATABASE_URL, future=True, echo=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def delete_database():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


async def create_database():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
