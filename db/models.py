__author__ = "AivanF"

from sqlalchemy import Index, Column, Integer, String, DateTime, JSON

from db.config import Base


class DevConnection(Base):
    __tablename__ = "dev_connection"

    id = Column(Integer, primary_key=True)
    dev1 = Column(String, nullable=False)
    dev2 = Column(String, nullable=False)
    registered_at = Column(DateTime, nullable=False)
    data = Column(JSON, nullable=False)

    idx_main = Index("dev1", "dev2", "id")
