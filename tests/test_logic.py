__author__ = "AivanF"

import asyncio
import pytest
import pytest_asyncio
from fastapi.testclient import TestClient

from app import app, processor
from adapters.github_mock import create_mock_github
from adapters.twitter_mock import create_mock_twitter
from db.config import delete_database, create_database
from utils import get_mock_data

processor.init(create_mock_github(), create_mock_twitter())

test_cases_realtime = get_mock_data()["test_cases_realtime"]
test_cases_register = get_mock_data()["test_cases_register"]

client = TestClient(app)


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="module")
@pytest.mark.asyncio
async def get_db():
    await delete_database()
    await create_database()


@pytest.mark.parametrize("test_case", test_cases_realtime)
@pytest.mark.asyncio
async def test_realtime_controller(get_db, test_case):
    got_org, got_err = await processor.get_connected_realtime(test_case["dev1"], test_case["dev2"])
    exp_org, exp_err = test_case.get("orgs", []), test_case.get("err", [])
    assert set(got_org) == set(exp_org)
    assert set(got_err) == set(exp_err)


@pytest.mark.parametrize("test_case", test_cases_realtime)
def test_realtime_app(get_db, test_case):
    # with TestClient(app) as client:
    response = client.get(f"/connected/realtime/{test_case['dev1']}/{test_case['dev2']}")
    exp_org, exp_err = test_case.get("orgs", []), test_case.get("err", [])
    exp_code = 404 if exp_err else 200
    exp_con = bool(exp_org)
    assert response.status_code == exp_code
    got_data = response.json()
    if not exp_err:
        assert got_data["connected"] == exp_con
    if exp_org:
        assert set(got_data["organisations"]) == set(exp_org)
    if exp_err:
        assert set(got_data["errors"]) == set(exp_err)


@pytest.mark.parametrize("test_case", test_cases_register)
def test_register_app(get_db, test_case):
    # with TestClient(app) as client:
    response = client.get(f"/connected/register/{test_case['dev1']}/{test_case['dev2']}")
    assert len(response.json()) == test_case["count"]
