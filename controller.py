__author__ = "AivanF"

from typing import Tuple, List

from adapters.github_abc import IGitHub, GitHubNoUserError
from adapters.twitter_abc import ITwitter, TwitterNoUserError


class Controller:
    """
    Allows to check developers connections on GitHub & Twitter
    """
    # Lazy initialization pattern is used
    github: IGitHub
    twitter: ITwitter

    def __init__(self):
        self.init_done = False

    def init(self, github: IGitHub, twitter: ITwitter):
        self.github = github
        self.twitter = twitter
        self.init_done = True

    async def get_connected_realtime(self, dev1: str, dev2: str) -> Tuple[List[str], List[str]]:
        # NOTE: in future it may be useful to develop more comprehensive error tracking logic,
        #   e.g. using Jinja template system.
        errors = []

        # Extract Twitter data
        try:
            following_12 = dev2 in await self.twitter.get_users_following(dev1)
        except TwitterNoUserError:
            following_12 = False
            errors.append("dev1 is no a valid user in twitter")
        try:
            following_21 = dev1 in await self.twitter.get_users_following(dev2)
        except TwitterNoUserError:
            following_21 = False
            errors.append("dev2 is no a valid user in twitter")

        # Extract GitHub data
        try:
            org_1 = await self.github.get_user_orgs(dev1)
        except GitHubNoUserError:
            org_1 = ()
            errors.append("dev1 is no a valid user in github")
        try:
            org_2 = await self.github.get_user_orgs(dev2)
        except GitHubNoUserError:
            org_2 = ()
            errors.append("dev2 is no a valid user in github")

        # Process results
        common_gh_org = ()
        if following_12 and following_21:
            # Use set to reduce time complexity to O(m+n)
            common_gh_org = list(set(org_1).intersection(org_2))
        return common_gh_org, errors
