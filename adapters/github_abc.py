__author__ = "AivanF"

from typing import Iterable
from abc import ABC, abstractmethod


class GitHubNoUserError(ValueError):
    pass


class IGitHub(ABC):
    @abstractmethod
    async def get_user_orgs(self, user_name: str) -> Iterable[str]:
        pass
