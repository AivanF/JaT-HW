__author__ = "AivanF"

from typing import Dict, Set, Iterable
from collections import defaultdict

from adapters.github_abc import IGitHub, GitHubNoUserError
from utils import get_mock_data


class GitHubMock(IGitHub):
    def __init__(self):
        self.user_orgs: Dict[str, Set[str]] = defaultdict(set)

    async def get_user_orgs(self, user_name: str) -> Iterable[str]:
        """
        Mock for this method:
        https://docs.github.com/en/rest/reference/orgs#list-organizations-for-a-user
        """
        if user_name not in self.user_orgs:
            raise GitHubNoUserError()
        return list(self.user_orgs[user_name])


def create_mock_github() -> GitHubMock:
    mock_github = GitHubMock()
    mock_data = get_mock_data()
    for user_name, user_data in mock_data["users"].items():
        if "github_org" in user_data:
            for org in user_data["github_org"]:
                mock_github.user_orgs[user_name].add(org)
    return mock_github
