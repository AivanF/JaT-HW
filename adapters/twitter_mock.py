__author__ = "AivanF"

from typing import Iterable
import networkx as nx

from adapters.twitter_abc import ITwitter, TwitterNoUserError
from utils import get_mock_data


class TwitterMock(ITwitter):
    def __init__(self):
        self.graph = nx.DiGraph()

    async def get_users_following(self, user_name: str) -> Iterable[str]:
        """
        Mock for this method:
        https://developer.twitter.com/en/docs/twitter-api/users/follows/api-reference/get-users-id-following
        For real-world implementation it may be needed
        to consider iterating by limited page size.
        """
        if user_name in self.graph:
            return list(self.graph.successors(user_name))
        else:
            raise TwitterNoUserError()


def create_mock_twitter() -> TwitterMock:
    mock_twitter = TwitterMock()
    mock_data = get_mock_data()
    pairs = [
        (user_name, target)
        for user_name, user_data in mock_data["users"].items()
        if "twitter_following" in user_data
        for target in user_data["twitter_following"]
    ]
    mock_twitter.graph.add_edges_from(pairs)
    return mock_twitter
