__author__ = "AivanF"

from typing import Iterable
from abc import ABC, abstractmethod


class TwitterNoUserError(ValueError):
    pass


class ITwitter(ABC):
    @abstractmethod
    async def get_users_following(self, user_name: str) -> Iterable[str]:
        pass
