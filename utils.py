__author__ = "AivanF"

from datetime import datetime
import yaml


def datetime_isoformat(dt: datetime):
    return f"{dt.isoformat(timespec='seconds')}Z"


def get_mock_data():
    with open("./mock_data.yaml") as f:
        return yaml.safe_load(f.read())
